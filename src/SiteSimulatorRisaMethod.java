public class SiteSimulatorRisaMethod extends SiteSimulator {

	protected int selectSite() {

		// Count TPPHs
		int numberOfTpphs = 0;
		for(String site : activeSites) {
			if (site.equals("TPPH")) {
				numberOfTpphs++;
			}
		}

		// <= 2 TPPHs: Do the best site; >2 TPPHs: Do a TPPH
		int i;
		if (numberOfTpphs <= 2) {
			if (this.activeSites.contains("TCRC")) {
				i = this.activeSites.indexOf("TCRC");
			} else if (this.activeSites.contains("NRF")) {
				i = this.activeSites.indexOf("NRF");
			} else {
				i = this.activeSites.indexOf("TPPH");
			}
		}
		else {
			i = this.activeSites.indexOf("TPPH");
		}

		return i;
	}
}