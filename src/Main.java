import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Main {

	private static final int SIMULATIONS_TO_RUN = 1000;
	private static final int SIMULATION_DURATION_IN_SECONDS = 60*60*24; // 24 hours of running

    public static void main(String[] args) {
	    NumberFormat formatter = new DecimalFormat("#0.00");

	    {
		    double iskPerHourSum = 0.00;
		    for (int i = 1; i <= SIMULATIONS_TO_RUN; i++) {
			    SiteSimulator simulator = new SiteSimulator();

			    while (simulator.getSecondsElapsed() < SIMULATION_DURATION_IN_SECONDS) {
				    simulator.simulateSecond();
			    }

			    iskPerHourSum += simulator.getIskPerHour();
		    }

		    System.out.println("RANDOM SELECT: Average ISK/hr of all Sims: " + formatter.format(iskPerHourSum / SIMULATIONS_TO_RUN));
	    }

	    {
		    double iskPerHourSum = 0.00;
		    for (int i = 1; i <= SIMULATIONS_TO_RUN; i++) {
			    SiteSimulator simulator = new SiteSimulatorBestSiteFirst();

			    while (simulator.getSecondsElapsed() < SIMULATION_DURATION_IN_SECONDS) {
				    simulator.simulateSecond();
			    }

			    iskPerHourSum += simulator.getIskPerHour();
		    }

		    System.out.println("BEST SITE SELECT: Average ISK/hr of all Sims: " + formatter.format(iskPerHourSum / SIMULATIONS_TO_RUN));
	    }

	    {
		    double iskPerHourSum = 0.00;
		    for (int i = 1; i <= SIMULATIONS_TO_RUN; i++) {
			    SiteSimulator simulator = new SiteSimulatorRisaMethod();

			    while (simulator.getSecondsElapsed() < SIMULATION_DURATION_IN_SECONDS) {
				    simulator.simulateSecond();
			    }

			    iskPerHourSum += simulator.getIskPerHour();
		    }

		    System.out.println("RISA SELECT: Average ISK/hr of all Sims: " + formatter.format(iskPerHourSum / SIMULATIONS_TO_RUN));
	    }

	    {
		    double iskPerHourSum = 0.00;
		    for (int i = 1; i <= SIMULATIONS_TO_RUN; i++) {
			    SiteSimulator simulator = new SiteSimulatorWorstSiteFirst();

			    while (simulator.getSecondsElapsed() < SIMULATION_DURATION_IN_SECONDS) {
				    simulator.simulateSecond();
			    }

			    iskPerHourSum += simulator.getIskPerHour();
		    }

		    System.out.println("WORST SITE SELECT: Average ISK/hr of all Sims: " + formatter.format(iskPerHourSum / SIMULATIONS_TO_RUN));
	    }
    }
}
