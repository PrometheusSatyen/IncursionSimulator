import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class SiteSimulator {

	protected ArrayList<String> activeSites;
	protected ArrayList<Integer> secondsAtWhichRespawnsDue;

	protected Integer secondsElapsed;
	protected long iskEarned;

	protected Integer secondsAtWhichCurrentSiteCompletes;

	public SiteSimulator() {
		this.activeSites = new ArrayList<>();
		this.secondsAtWhichRespawnsDue = new ArrayList<>();
		this.secondsElapsed = 0;
		this.iskEarned = 0;

		for(int i = 0; i < 5; i++) {
			this.spawnSite();
		}

		this.startSite(this.selectSite());
	}

	protected int selectSite() {
		return ThreadLocalRandom.current().nextInt(0, this.activeSites.size());
	}

	protected void startSite(int index) {
		String site = this.activeSites.get(index);
		this.activeSites.remove(index);

		switch(site) {
			case "TCRC":
				this.secondsAtWhichCurrentSiteCompletes = this.secondsElapsed + 60*7; // 7 min TCRC
				break;
			case "NRF":
				this.secondsAtWhichCurrentSiteCompletes = this.secondsElapsed + 60*11; // 11 min NRF
				break;
			case "TPPH":
				this.secondsAtWhichCurrentSiteCompletes = this.secondsElapsed + 60*16; // 16 min TPPH
				break;
		}
	}

	protected void spawnSite() {
		int i = ThreadLocalRandom.current().nextInt(1, 4);
		String site = "";

		switch(i) {
			case 1:
				site = "TCRC";
				break;
			case 2:
				site = "NRF";
				break;
			case 3:
				site = "TPPH";
				break;
		}

		this.activeSites.add(site);
	}

	public void simulateSecond() {
		this.secondsElapsed = this.secondsElapsed + 1;

		if (this.secondsAtWhichRespawnsDue.contains(this.secondsElapsed)) {
			this.spawnSite();
			this.secondsAtWhichRespawnsDue.remove(this.secondsElapsed);
		}

		if (this.secondsAtWhichCurrentSiteCompletes.equals(this.secondsElapsed)) {
			this.iskEarned += 31500000;
			this.secondsAtWhichRespawnsDue.add(this.secondsElapsed + 450); // 7.5 min respawn
			this.startSite(this.selectSite());
		}
	}

	public double getIskPerHour() {
		return this.iskEarned / (this.secondsElapsed / 3600);
	}

	public int getSecondsElapsed() {
		return this.secondsElapsed;
	}
}
