public class SiteSimulatorBestSiteFirst extends SiteSimulator {

	protected int selectSite() {
		int i;

		// Do the best site on grid
		if (this.activeSites.contains("TCRC")) {
			i = this.activeSites.indexOf("TCRC");
		} else if (this.activeSites.contains("NRF")) {
			i = this.activeSites.indexOf("NRF");
		} else {
			i = this.activeSites.indexOf("TPPH");
		}

		return i;
	}
}
