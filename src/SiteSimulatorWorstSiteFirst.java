public class SiteSimulatorWorstSiteFirst extends SiteSimulator {

	protected int selectSite() {
		int i;

		// Do the worst site on grid
		if (this.activeSites.contains("TPPH")) {
			i = this.activeSites.indexOf("TPPH");
		} else if (this.activeSites.contains("NRF")) {
			i = this.activeSites.indexOf("NRF");
		} else {
			i = this.activeSites.indexOf("TCRC");
		}

		return i;
	}
}
